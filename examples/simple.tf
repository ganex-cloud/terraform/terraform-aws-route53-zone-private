module "route53-domain-local-zone" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-route53-zone-private.git?ref=master"
  domain = "domain.local"
}
