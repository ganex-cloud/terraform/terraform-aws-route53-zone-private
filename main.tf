# Domain
resource "aws_route53_zone" "private" {
  name = "${var.domain}"

  vpc {
    vpc_id = "${var.vpc}"
  }
}
