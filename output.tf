output "zone_id" {
  description = "The Hosted Zone Private ID. This can be referenced by zone records."
  value       = "${aws_route53_zone.private.zone_id}"
}

output "name_servers" {
  description = "Name Server (NS) records."
  value       = "${join(",", aws_route53_zone.private.name_servers)}"
}
