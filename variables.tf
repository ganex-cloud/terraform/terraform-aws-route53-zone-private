variable "domain" {
  description = "Domain zone name"
}

variable "vpc" {
  description = "VPC ID from new zone private"
}
